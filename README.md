# Install LAMP though PUPPET Agent with Vagrant#

This README would normally document whatever steps are necessary to get your application up and running.

### Create pp-lamp repo ###

### Install Puppet Agent ###

* Set time:
	$ sudo timedatectl set-timezone Africa/Nairobi
* Set hostname:
	$ sudo hostnamectl set-hostname agent.pp.local
* Install Puppet-agent
	$ wget https://apt.puppetlabs.com/puppet-release-bionic.deb
	$ sudo dpkg -i puppet-release-bionic.deb 
	$ sudo apt-get install puppet
	

### Write puppet code ###

* Add module
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact